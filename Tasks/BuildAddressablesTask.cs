﻿using System.Collections;
using System.IO;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

public class BuildAddressablesTask : AutomatorTaskBase
{
    public override string[] ConfigParameters { get; } = new string[0];
    
    public override IEnumerator Prepare()
    {
        yield break;
    }
    
    public override IEnumerator Configure(Hashtable data)
    {
        if (CustomParameter.ToLowerInvariant() != "dontclear")
            ClearBuild();
        yield return new WaitForSecondsRealtime(1);
        if (!BuildAddressables())
        {
            yield return Automator.FAILURE;
            yield break;
        }
        yield return 0;
    }

    private void ClearBuild()
    {
        AddressableAssetSettings.CleanPlayerContent();
        if(Directory.Exists("Library/BuildCache"))
            Directory.Delete("Library/BuildCache", true);
    }
    private bool BuildAddressables()
    {
        AddressableAssetSettings
            .BuildPlayerContent(out AddressablesPlayerBuildResult result);
        bool success = string.IsNullOrEmpty(result.Error);

        if (!success)
        {
            Debug.LogError("Addressables build error encountered: " + result.Error);
        }
       
        return success;
    }
}